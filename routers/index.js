const express = require('express');
const router = express.Router();
const speedTest = require('speedtest-net');

router.get('/', (req , res) => {
    const myCustomCookie = req.cookies.myCustomCookie;
    return res.render('index', {
        PageTitle: 'Home Page |  Weather App',
        message: null,
        myCustomCookie,
        weather: null
    });
});

router.get('/contact-form' ,(req ,res) => {

    return res.render('ContactForm' ,{
        PageTitle: 'Contact Form Page |  Weather App',
        message: null,
    });
})
router.get('/About' ,(req ,res) => {

    return res.render('About' ,{
        PageTitle: 'About Us Page |  Weather App',
        message: null,
    });
})

router.get('/check-speed' ,(req,res) => {
    return res.render('SpeedTest' ,{
        PageTitle: 'Net Speed Test |  Weather App',
        message: null,
        downloadSpeed: null,
        uploadSpeed:null  
    });
});



router.get('/speedtest', (req, res) => {
    try{
    const speed = speedTest({ acceptLicense: true, maxTime: 5000 })
      .then((data) => {
            const downloadSpeed = data.download.bandwidth;
            const uploadSpeed = data.upload.bandwidth;
            return res.render('SpeedTest', { 
                downloadSpeed: formatSpeed(downloadSpeed), 
                uploadSpeed:   formatSpeed(uploadSpeed) ,
                PageTitle: 'Net Speed Test |  Weather App', 
                message:null 
            });
      })
      .catch((error) => {
        console.error('Speedtest error:', error);
        return res.render('SpeedTest' ,{
            PageTitle: 'Net Speed Test |  Weather App',
            message: `Speedtest failed: ${error.message}`,
            downloadSpeed: null,
            uploadSpeed: null
        });
      });
    }catch(error){
     console.log(`Error In Speed Test :${error}`);
     return res.render('SpeedTest' ,{
         PageTitle: 'Net Speed Test |  Weather App',
         message: `Error In Check Speed : ${error.message}`,
         downloadSpeed: null,
         uploadSpeed: null
     });
    }
 });


 function formatSpeed(speed) {
  if (speed >= 1000000) {
    return (speed / 1000000).toFixed(2) + " Mbps";
  } else if (speed >= 1000) {
    return (speed / 1000).toFixed(2) + " Kbps";
  } else {
    return speed.toFixed(2) + " bps";
  }
}


router.use('/user' , require('./AuthRoutes'));

router.use('/form' ,require('./ContactForm'));

router.use('/new', require('./Weather'));

module.exports = router;
